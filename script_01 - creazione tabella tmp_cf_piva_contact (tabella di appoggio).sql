SET SQL_SAFE_UPDATES = 0;
SET innodb_lock_wait_timeout = 10000;
SET sql_mode="";
##########################################################################################
#CREAZIONE TABELLA dei nomi random (per ogni tupla si inserisce un nome univoco)
Create table RandomNames
(
 id int,
 nomeRandom varchar(255),
 cognomeRandom varchar(100),
 cfPrimeSeiCifre varchar(6),
 gender char(1)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
;
insert into RandomNames
(id, nomeRandom, cognomeRandom, gender, cfPrimeSeiCifre)
select 1,'Jack', 'Percoco', 'M', 'PRCJCK'
union
select 2,'Ambrose', 'Trozzo', 'M', 'TRZMRS'
union
select 3,'Bastian','Berlinato', 'M', 'BRLBTN'
union
select 4,'Caleb', 'Liotto', 'M', 'LTTCLB'
union
select 5,'Cliff', 'Gengio', 'M', 'GNGCFF'
union
select 6,'Alannis', 'Zabba', 'F', ' ZBBLNS'
union
select 7,'Anjelica', 'Diuroto', 'F', 'DRTNLC'
union
select 8,'Brenda', 'Elipto', 'F', 'LPTBND'
union
select 9,'Carrie', 'Plessotico', 'F', 'PLSCRR'
union
select 10,'Darla', 'Gengeto', 'F', 'GNGDRL'
;
##########################################################################################
#CREAZIONE TABELLA tmp_cf_piva_contact
CREATE TABLE tmp_cf_piva_contact (
  contactid int(19) NOT NULL DEFAULT '0',
  nome_old varchar(255)  NULL,
  cognome_old varchar(100)  NULL,
  nome_new varchar(255)  NULL,
  cognome_new varchar(100)  NULL,
  cf_old varchar(20)   NULL,
  cf_new varchar(20)   NULL,
  piva_old varchar(255)  NULL,
  piva_new varchar(255)  NULL,
  sesso varchar(1)  NULL,
  PRIMARY KEY (`contactid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
;
ALTER TABLE `tmp_cf_piva_contact` ADD INDEX `cf_old` (`cf_old`)
;
ALTER TABLE `tmp_cf_piva_contact` ADD INDEX `cf_new` (`cf_new`)
;
ALTER TABLE `tmp_cf_piva_contact` ADD INDEX `piva_old` (`piva_old`)
;
ALTER TABLE `tmp_cf_piva_contact` ADD INDEX `piva_new` (`piva_new`)
;
##########################################################################################
#inserimento nella tabella tmp_cf_piva_contact delle info originali del cliente
INSERT INTO tmp_cf_piva_contact (contactid, cognome_old, nome_old, cf_old, piva_old, sesso)
	select 
		det.contactid, det.lastname, det.firstname, cf.cf_1391 as cf, cf.cf_1799 as piva, cf.cf_1817 as sesso
	from 
		vtiger_contactdetails det, vtiger_contactscf cf
	where 
		det.contactid = cf.contactid
		and det.contactid not in 
							(select contactid
                             from tmp_cf_piva_contact
							)
;
##########################################################################################
#aggiornamento del nome_new
#a)donne (se la colonna sesso non era presente nella tabella vtiger_contactscf si assegna come sesso donna)
update
	tmp_cf_piva_contact
set 
    nome_new = (SELECT nomeRandom FROM RandomNames WHERE gender = 'F' ORDER BY RAND() LIMIT 1)
where
	sesso = 'F' or sesso is null or sesso = ''
;
#b)uomini
update
	tmp_cf_piva_contact
set 
    nome_new = (SELECT nomeRandom FROM RandomNames WHERE gender = 'M' ORDER BY RAND() LIMIT 1)
where
	sesso = 'M'
;
##########################################################################################
#aggiornamento del cognome_new
update
	tmp_cf_piva_contact T
set 
    T.cognome_new = (SELECT cognomeRandom FROM RandomNames n where T.nome_new = n.nomeRandom LIMIT 1)
;
##########################################################################################
#aggiornamento del cf_new - piva_new
#a)cf_old di 16 cifre
update
	tmp_cf_piva_contact T
set 
    T.cf_new = concat( (SELECT cfPrimeSeiCifre FROM RandomNames n where T.nome_new = n.nomeRandom LIMIT 1), SUBSTR(T.cf_old,7,16) )
where
	length(T.cf_old) = 16
;
#b)piva_old di 16 cifre AND piva_old = cf_old
update
	tmp_cf_piva_contact T
set 
    T.piva_new = T.cf_new
where
	length(T.piva_old) = 16 and piva_old = cf_old
;
#c)cf_old <> 16 cifre and (cf_old is null or piva_old is null)
update
	tmp_cf_piva_contact T
set 
    T.cf_new = '11111111111', T.piva_new = '11111111111'
where
	length(T.cf_old) <> 16 and (cf_old is null or piva_old is null)
;
#d)se piva_new is null => T.piva_new = '11111111111'
update
	tmp_cf_piva_contact T
set 
    T.piva_new = '11111111111'
where
	T.piva_new is null or TRIM(T.piva_new) = ''
;
