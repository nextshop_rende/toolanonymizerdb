SET SQL_SAFE_UPDATES = 0;
SET innodb_lock_wait_timeout = 10000;
SET sql_mode="";





########################################################################################## MODULO CONTACT
#1)vtiger_contactdetails
update
	vtiger_contactdetails t
set
	t.firstname  = (select nome_new from tmp_cf_piva_contact c where c.contactid =  t.contactid)
where
	t.firstname is not null and trim(t.firstname) <> ''
;
update
	vtiger_contactdetails t
set
	t.lastname  = (select cognome_new from tmp_cf_piva_contact c where c.contactid =  t.contactid)
where
	t.lastname is not null and trim(t.lastname) <> ''
;
update
	vtiger_contactdetails t
set
	t.email = 'fake@fake.com'
where
	t.email is not null and trim(t.email) <> ''
;
update
	vtiger_contactdetails t
set
	t.mobile = '3123456789'
where
	t.mobile is not null and trim(t.mobile) <> ''
;
update
	vtiger_contactdetails t
set
	t.phone = '01234567'
where
	t.phone is not null and trim(t.phone) <> ''
;
##########################2)vtiger_contactscf 
#cf_1391 = > codice fiscale
update
	vtiger_contactscf t
set 
    t.cf_1391 = (select cf_new from tmp_cf_piva_contact c where c.contactid =  t.contactid)
where
	t.cf_1391 is not null and trim(t.cf_1391) <> '' 
;    
#cf_1799 = > partita iva
update
	vtiger_contactscf t
set 
    t.cf_1799 = (select piva_new from tmp_cf_piva_contact c where c.contactid =  t.contactid)
where
	t.cf_1799 is not null and trim(t.cf_1799) <> '' 
;    
#cf_1855 = > IBAN
update
	vtiger_contactscf t
set 
    t.cf_1855 = 'IT02L1234512345123456789012'
where
	t.cf_1855 is not null and trim(t.cf_1855) <> '' 
;   
#username
update
	vtiger_contactscf t
set 
    t.username = (select lower(concat(concat(c.firstname,'.'),c.lastname)) 
				  from vtiger_contactdetails c 
                  where c.contactid = t.contactid
                  and c.firstname is not null and c.lastname is not null
                  )
where
	(t.username is not null and trim(t.username) <> '')  
; 





########################################################################################## MODULO LEAD
##########################1)vtiger_leadscf
#cf_1869 => (codice fiscale mappato con modulo Contact)
update
    (
		select c.cf_old, c.cf_new, t.leadid, t.cf_1869
		from vtiger_leadscf t 
		join tmp_cf_piva_contact c on (t.cf_1869 = c.cf_old)
		where t.cf_1869 is not null and trim(t.cf_1869) <> ''
    )s
    join vtiger_leadscf t2 on (s.leadid = t2.leadid)
set 
	t2.cf_1869 = s.cf_new
;
#cf_1871 => (partita iva mappato con modulo Contact)
update
    (
		select c.piva_new, t.leadid
		from vtiger_leadscf t 
		join tmp_cf_piva_contact c on (t.cf_1871 = c.piva_old)
        where t.cf_1871 and trim(t.cf_1871) <> ''
    )s
    join vtiger_leadscf t2 on (s.leadid = t2.leadid)
set t2.cf_1871 = s.piva_new
;
#cf_1869 => (codice fiscale)
CREATE TEMPORARY TABLE IF NOT EXISTS 
  temp_table ( INDEX(leadid) ) 
AS (
						select t2.leadid
						from vtiger_leadscf t2 
						join tmp_cf_piva_contact c on (t2.cf_1869 = c.cf_new)
						where t2.cf_1869 and trim(t2.cf_1869) <> '' and t2.cf_1869 <> '11111111111'
)
;
update
	vtiger_leadscf t 
set 
	t.cf_1869 = '11111111111'
where
	t.leadid not in (
						select leadid from temp_table
					)				
	and t.cf_1869 is not null and trim(t.cf_1869) <> ''
;
drop table temp_table
;
#cf_1871 => (partita iva)
CREATE TEMPORARY TABLE IF NOT EXISTS 
  temp_table ( INDEX(leadid) ) 
AS (
						select t2.leadid
						from vtiger_leadscf t2 
						join tmp_cf_piva_contact c on (t2.cf_1871 = c.piva_new)
						where t2.cf_1871 and trim(t2.cf_1871) <> '' and t2.cf_1871 <> '11111111111'
)
;
update
	vtiger_leadscf t 
set 
	t.cf_1871 = '11111111111'
where
	t.leadid not in (
						select leadid from temp_table
					)				
	and t.cf_1871 is not null and trim(t.cf_1871) <> ''
;
drop table temp_table
;
#cf_1759 => (email del Web Account)
update
	vtiger_leadscf t 
set
	t.cf_1759 = 'fake@fake.com'
where
	t.cf_1759 is not null and trim(t.cf_1759) <> ''
;
#email
update
	vtiger_leaddetails t 
set
	t.email = 'fake@fake.com'
where
	t.email is not null and trim(t.email) <> ''
;
#cf_1715 => (email iscritto)
update
	vtiger_contactscf
set
	cf_1715 = '999999@siae.it'	
where 
	cf_1715 is not null and trim(cf_1715) <> ''
;    
#cf_1893 => (PEC)
update
	vtiger_leadscf t 
set
	t.cf_1893 = 'fake@pec.com'
where
	t.cf_1893 is not null and trim(t.cf_1893) <> ''
;
##########################2)vtiger_leaddetails 
#firstname (mappaggio generico per sesso femminile)
update
	vtiger_leaddetails t
set 
    t.firstname = (SELECT nomeRandom FROM RandomNames WHERE gender = 'F' ORDER BY RAND() LIMIT 1)
where
	t.firstname is not null and trim(t.firstname) <> '' 
	and t.leadid in (select c.leadid from vtiger_leadscf c where trim(c.cf_1877) = 'F' or c.cf_1877 is null or trim(c.cf_1877) = '' or trim(c.cf_1877) <> 'M')
;   
#firstname (mappaggio generico per sesso maschile)
update
	vtiger_leaddetails t
set 
    t.firstname = (SELECT nomeRandom FROM RandomNames WHERE gender = 'M' ORDER BY RAND() LIMIT 1)
where
	t.firstname is not null and trim(t.firstname) <> '' 
	and t.leadid in (select c.leadid from vtiger_leadscf c where trim(c.cf_1877) = 'M')
;  
#firstname (mappaggio con i clienti tramite colonna codice fiscale [cf_1869])
update
    (
		select t2.firstname, t1.cf_1869, t1.leadid, t3.nome_new
		from vtiger_leadscf t1 
		join vtiger_leaddetails t2 on (t1.leadid = t2.leadid)
		join tmp_cf_piva_contact t3 on (t1.cf_1869 = t3.cf_new)
		where t1.cf_1869 is not null and trim(t1.cf_1869) <> '' and t1.cf_1869 <> '11111111111'
    )s
    join vtiger_leaddetails t on (s.leadid = t.leadid)
set 
	t.firstname = s.nome_new
;
#firstname (mappaggio con i clienti tramite colonna partita iva [cf_1871])
update
    (
		select t2.firstname, t1.cf_1871, t1.leadid, t3.nome_new
		from vtiger_leadscf t1 
		join vtiger_leaddetails t2 on (t1.leadid = t2.leadid)
		join tmp_cf_piva_contact t3 on (t1.cf_1871 = t3.piva_new)
		where t1.cf_1871 is not null and trim(t1.cf_1871) <> '' and t1.cf_1871 <> '11111111111'
    )s
    join vtiger_leaddetails t on (s.leadid = t.leadid)
set 
	t.firstname = s.nome_new
;
#lastname (mappaggio generico)
update
	vtiger_leaddetails t
set 
    t.lastname = (SELECT cognomeRandom FROM RandomNames n where t.firstname = n.nomeRandom LIMIT 1)
where
	t.lastname is not null and trim(t.lastname) <> '' 
;  
#lastname (mappaggio con i clienti tramite colonna codice fiscale [cf_1869])
update
    (
		select t2.lastname, t1.cf_1869, t1.leadid, t3.cognome_new
		from vtiger_leadscf t1 
		join vtiger_leaddetails t2 on (t1.leadid = t2.leadid)
		join tmp_cf_piva_contact t3 on (t1.cf_1869 = t3.cf_new)
		where t1.cf_1869 is not null and trim(t1.cf_1869) <> '' and t1.cf_1869 <> '11111111111'
    )s
    join vtiger_leaddetails t on (s.leadid = t.leadid)
set 
	t.lastname = s.cognome_new
;
#lastname (mappaggio con i clienti tramite colonna codice fiscale [cf_1871])
update
    (
		select t2.lastname, t1.cf_1871, t1.leadid, t3.cognome_new
		from vtiger_leadscf t1 
		join vtiger_leaddetails t2 on (t1.leadid = t2.leadid)
		join tmp_cf_piva_contact t3 on (t1.cf_1871 = t3.cf_new)
		where t1.cf_1871 is not null and trim(t1.cf_1871) <> '' and t1.cf_1871 <> '11111111111'
    )s
    join vtiger_leaddetails t on (s.leadid = t.leadid)
set 
	t.lastname = s.cognome_new
;
##########################3)vtiger_leadaddress
update
	vtiger_leadaddress
set
	phone = '01234567'
where
	phone is not null and trim(phone) <> ''
;
update
	vtiger_leadaddress
set
	mobile = '3123456789'
where
	mobile is not null and trim(mobile) <> ''
;
update
	vtiger_leadaddress
set
	phone = '01234567'
where
	phone is not null and trim(phone) <> ''
;





########################################################################################## MODULO ADESIONI SIAE
##########################1)vtiger_adesioni
#ade_cf => (codice fiscale mappato con modulo Contact)
update
    (
		select 
			t.ade_cf, c.cf_old, t.adesioni_id, c.cf_new
		from 
			vtiger_adesioni t 
			join tmp_cf_piva_contact c on (t.ade_cf = c.cf_old and c.contactid = t.ade_parent_id)
		where 
			t.ade_cf is not null and trim(t.ade_cf) <> ''
			and t.ade_id_tipo_richiesta = 1
    )s
    join vtiger_adesioni t2 on (s.adesioni_id = t2.adesioni_id)
set 
	t2.ade_cf = s.cf_new
;
#ade_piva => (partita iva mappato con modulo Contact)
update
    (
		select 
			t.ade_piva, c.piva_old, t.adesioni_id, c.piva_new
		from 
			vtiger_adesioni t 
			join tmp_cf_piva_contact c on (t.ade_piva = c.piva_old and c.contactid = t.ade_parent_id)
		where 
			t.ade_piva is not null and trim(t.ade_piva) <> ''
			and t.ade_id_tipo_richiesta = 1
    )s
    join vtiger_adesioni t2 on (s.adesioni_id = t2.adesioni_id)
set 
	t2.ade_piva = s.piva_new
;
#ade_cf => (codice fiscale)
CREATE TEMPORARY TABLE IF NOT EXISTS 
  temp_table ( INDEX(adesioni_id) ) 
AS (						
						select
							t.adesioni_id
						from 
							vtiger_adesioni t 
							join tmp_cf_piva_contact c on (t.ade_cf = c.cf_new and c.contactid = t.ade_parent_id)
						where 
							t.ade_cf is not null and trim(t.ade_cf) <> '' and t.ade_cf <> '11111111111'
)
;
update
	vtiger_adesioni t 
set 
	t.ade_cf = '11111111111'
where
	t.adesioni_id not in (
						select adesioni_id from temp_table
					)				
	and t.ade_cf is not null and trim(t.ade_cf) <> ''
;
drop table temp_table
;
#ade_piva => (partita iva)
CREATE TEMPORARY TABLE IF NOT EXISTS 
  temp_table ( INDEX(adesioni_id) ) 
AS (						
						select
							t.adesioni_id
						from 
							vtiger_adesioni t 
							join tmp_cf_piva_contact c on (t.ade_piva = c.piva_new and c.contactid = t.ade_parent_id)
						where 
							t.ade_piva is not null and trim(t.ade_piva) <> '' and t.ade_piva <> '11111111111'
)
;
update
	vtiger_adesioni t 
set 
	t.ade_piva = '11111111111'
where
	t.adesioni_id not in (
						select adesioni_id from temp_table
					)				
	and t.ade_piva is not null and trim(t.ade_piva) <> ''
;
drop table temp_table
;
#ade_nome (mappaggio generico)
update
	vtiger_adesioni t
set 
    t.ade_nome = (SELECT nomeRandom FROM RandomNames ORDER BY RAND() LIMIT 1)
where
	t.ade_nome is not null and trim(t.ade_nome) <> '' 
; 
#ade_cognome (mappaggio generico)
update
	vtiger_adesioni t
set 
    t.ade_cognome = (SELECT cognomeRandom FROM RandomNames ORDER BY RAND() LIMIT 1)
where
	t.ade_cognome is not null and trim(t.ade_cognome) <> '' 
; 
#ade_nome, ade_cognome
update
	vtiger_adesioni t
set 
    t.ade_nome = (SELECT n.nomeRandom FROM RandomNames n where n.cfPrimeSeiCifre = substring(t.ade_cf,1,6)),
    t.ade_cognome = (SELECT n.cognomeRandom FROM RandomNames n where n.cfPrimeSeiCifre = substring(t.ade_cf,1,6))
where
	t.ade_nome is not null and trim(t.ade_nome) <> '' 
    and t.ade_cognome is not null and trim(t.ade_cognome) <> '' 
; 
#ade_email => (email del Web Account)
update
	vtiger_adesioni t 
set
	t.ade_email = 'fake@fake.com'
where
	t.ade_email is not null and trim(t.ade_email) <> ''
;
#ade_username
update
	vtiger_adesioni t
set 
    t.ade_username = lower(concat(concat(t.ade_nome,'.'),t.ade_cognome)) 
where
	t.ade_username is not null and trim(t.ade_username) <> ''
    and t.ade_nome is not null and t.ade_cognome is not null
; 





########################################################################################## MODULO ISCRIZIONI OFFLINE
##########################1)vtiger_iscrizioneoffline
#iof_cf => (codice fiscale mappato con modulo Contact)
update
    (
		select 
			t.iof_cf, c.cf_old, t.iscrizioneoffline_id, c.cf_new
		from 
			vtiger_iscrizioneoffline t 
			join tmp_cf_piva_contact c on (t.iof_cf = c.cf_old)
		where 
			t.iof_cf is not null and trim(t.iof_cf) <> ''
    )s
    join vtiger_iscrizioneoffline t2 on (s.iscrizioneoffline_id = t2.iscrizioneoffline_id)
set 
	t2.iof_cf = s.cf_new
;
#iof_piva => (partita iva mappato con modulo Contact)
update
    (
		select 
			t.iof_piva, c.piva_old, t.iscrizioneoffline_id, c.piva_new
		from 
			vtiger_iscrizioneoffline t 
			join tmp_cf_piva_contact c on (t.iof_piva = c.piva_old)
		where 
			t.iof_piva is not null and trim(t.iof_piva) <> ''
    )s
    join vtiger_iscrizioneoffline t2 on (s.iscrizioneoffline_id = t2.iscrizioneoffline_id)
set 
	t2.iof_piva = s.piva_new
;
#iof_cf => (codice fiscale)
CREATE TEMPORARY TABLE IF NOT EXISTS 
  temp_table ( INDEX(iscrizioneoffline_id) ) 
AS (						
						select
							t.iscrizioneoffline_id
						from 
							vtiger_iscrizioneoffline t 
							join tmp_cf_piva_contact c on (t.iof_cf = c.cf_new)
						where 
							t.iof_cf is not null and trim(t.iof_cf) <> '' and t.iof_cf <> '11111111111'
)
;
update
	vtiger_iscrizioneoffline t 
set 
	t.iof_cf = '11111111111'
where
	t.iscrizioneoffline_id not in (
						select iscrizioneoffline_id from temp_table
					)				
	and t.iof_cf is not null and trim(t.iof_cf) <> ''
;
drop table temp_table
;
#iof_piva => (prtita iva)
CREATE TEMPORARY TABLE IF NOT EXISTS 
  temp_table ( INDEX(iscrizioneoffline_id) ) 
AS (						
						select
							t.iscrizioneoffline_id
						from 
							vtiger_iscrizioneoffline t 
							join tmp_cf_piva_contact c on (t.iof_piva = c.piva_new)
						where 
							t.iof_cf is not null and trim(t.iof_piva) <> '' and t.iof_piva <> '11111111111'
)
;
update
	vtiger_iscrizioneoffline t 
set 
	t.iof_piva = '11111111111'
where
	t.iscrizioneoffline_id not in (
						select iscrizioneoffline_id from temp_table
					)				
	and t.iof_piva is not null and trim(t.iof_piva) <> ''
;
drop table temp_table
;
#iof_nome (mappaggio generico)
update
	vtiger_iscrizioneoffline t
set 
    t.iof_nome = (SELECT nomeRandom FROM RandomNames ORDER BY RAND() LIMIT 1)
where
	t.iof_nome is not null and trim(t.iof_nome) <> '' 
; 
#iof_cognome (mappaggio generico)
update
	vtiger_iscrizioneoffline t
set 
    t.iof_cognome = (SELECT cognomeRandom FROM RandomNames ORDER BY RAND() LIMIT 1)
where
	t.iof_cognome is not null and trim(t.iof_cognome) <> '' 
; 
#iof_nome, iof_cognome
update
	vtiger_iscrizioneoffline t
set 
    t.iof_nome = (SELECT n.nomeRandom FROM RandomNames n where n.cfPrimeSeiCifre = substring(t.iof_cf,1,6)),
    t.iof_cognome = (SELECT n.cognomeRandom FROM RandomNames n where n.cfPrimeSeiCifre = substring(t.iof_cf,1,6))
where
	t.iof_nome is not null and trim(t.iof_nome) <> '' 
    and t.iof_cognome is not null and trim(t.iof_cognome) <> '' 
; 
#iof_email => (email del Web Account)
update
	vtiger_iscrizioneoffline t 
set
	t.iof_email = 'fake@fake.com'
where
	t.iof_email is not null and trim(t.iof_email) <> ''
;
#iof_iban
update
	vtiger_iscrizioneoffline t
set 
    t.iof_iban = 'IT02L1234512345123456789012'
where
	t.iof_iban is not null and trim(t.iof_iban) <> '' 
;  





########################################################################################## MODULO RECAPITI
##########################1)vtiger_recapiti
update
	vtiger_recapiti
set 
	recapito = 'fake'
where
	id_tipo_recapito is null or id_tipo_recapito = 'INT'
;	
update
	vtiger_recapiti
set 
	recapito = '3123456789'
where
	id_tipo_recapito = 'CELLULARE'
;	
update
	vtiger_recapiti
set 
	recapito = 'www.fake.fake'
where
	id_tipo_recapito = 'SITO'
;	
update
	vtiger_recapiti
set 
	recapito = '01234567'
where
	id_tipo_recapito = 'TEL' or id_tipo_recapito = 'TELEFONO' or id_tipo_recapito = 'FAX'
;	
update
	vtiger_recapiti
set 
	recapito = 'fake@fake.com'
where
	id_tipo_recapito = 'EMAIL' or id_tipo_recapito = 'WEBACC'
;		





########################################################################################## Modulo Posizioni SIAE
##########################1)vtiger_posizioni_siae
#nome_intestatario_pagamento (mappaggio generico)
update
	vtiger_posizioni_siae t
set 
    t.nome_intestatario_pagamento = (SELECT nomeRandom FROM RandomNames ORDER BY RAND() LIMIT 1)
where
	t.nome_intestatario_pagamento is not null and trim(t.nome_intestatario_pagamento) <> '' 
; 
#cognome_intestatario_pagamento (mappaggio generico)
update
	vtiger_posizioni_siae t
set 
    t.cognome_intestatario_pagamento = (SELECT cognomeRandom FROM RandomNames ORDER BY RAND() LIMIT 1)
where
	t.cognome_intestatario_pagamento is not null and trim(t.cognome_intestatario_pagamento) <> '' 
; 
#nome_intestatario_pagamento, cognome_intestatario_pagamento (mappaggio con nome del modulo Contact)
update
    (
		SELECT c.contactid, p.id_customer, p.cognome_intestatario_pagamento, nome_intestatario_pagamento, c.lastname, c.firstname   
		FROM vtiger_posizioni_siae p, vtiger_contactdetails c  
		where c.contactid = p.id_customer
    )s
    join vtiger_posizioni_siae t2 on (s.id_customer = t2.id_customer)
set 
	t2.nome_intestatario_pagamento = s.firstname,
	t2.cognome_intestatario_pagamento = s.lastname
;
#email
update
	vtiger_posizioni_siae t 
set
	t.email = 'fake@fake.com'
where
	t.email is not null and trim(t.email) <> ''
;
#iban
update
	vtiger_posizioni_siae t 
set
	t.iban = 'IT02L1234512345123456789012'
where
	t.iban is not null and trim(t.iban) <> ''
;
#numero_cc
update
	vtiger_posizioni_siae t 
set
	t.numero_cc = '000000000000'
where
	t.numero_cc is not null and trim(t.numero_cc) <> ''
;





########################################################################################## Modulo Registrazione Clienti
##########################1)vtiger_registrazione_clienti
#rc_p_codice_fiscale => (codice fiscale mappato con modulo Contact)
update
    (
		select 
			t.rc_p_codice_fiscale, c.cf_old, t.registrazioneclienti_id, c.cf_new
		from 
			vtiger_registrazione_clienti t 
			join tmp_cf_piva_contact c on (t.rc_p_codice_fiscale = c.cf_old)
		where 
			t.rc_p_codice_fiscale is not null and trim(t.rc_p_codice_fiscale) <> ''
    )s
    join vtiger_registrazione_clienti t2 on (s.registrazioneclienti_id = t2.registrazioneclienti_id)
set 
	t2.rc_p_codice_fiscale = s.cf_new
;
#rc_p_codice_fiscale => (codice fiscale)
CREATE TEMPORARY TABLE IF NOT EXISTS 
  temp_table ( INDEX(registrazioneclienti_id) ) 
AS (						
						select
							t.registrazioneclienti_id
						from 
							vtiger_registrazione_clienti t 
							join tmp_cf_piva_contact c on (t.rc_p_codice_fiscale = c.cf_new)
						where 
							t.rc_p_codice_fiscale is not null and trim(t.rc_p_codice_fiscale) <> '' and t.rc_p_codice_fiscale <> '11111111111'
)
;
update
	vtiger_registrazione_clienti t 
set 
	t.rc_p_codice_fiscale = '11111111111'
where
	t.registrazioneclienti_id not in (
						select registrazioneclienti_id from temp_table
					)				
	and t.rc_p_codice_fiscale is not null and trim(t.rc_p_codice_fiscale) <> ''
;
drop table temp_table
;
#rc_p_nome (mappaggio generico per sesso femminile)
update
	vtiger_registrazione_clienti t
set 
    t.rc_p_nome = (SELECT nomeRandom FROM RandomNames WHERE gender = 'F' ORDER BY RAND() LIMIT 1)
where
	t.rc_p_nome is not null and trim(t.rc_p_nome) <> '' 
	and (t.rc_p_sesso = 'F' or t.rc_p_sesso is null or trim(t.rc_p_sesso) <> 'M')
; 
#rc_p_nome (mappaggio generico per sesso maschile)
update
	vtiger_registrazione_clienti t
set 
    t.rc_p_nome = (SELECT nomeRandom FROM RandomNames WHERE gender = 'M' ORDER BY RAND() LIMIT 1)
where
	t.rc_p_nome is not null and trim(t.rc_p_nome) <> '' 
	and (t.rc_p_sesso = 'M')
; 
#rc_p_cognome (mappaggio generico)
update
	vtiger_registrazione_clienti t
set 
    t.rc_p_cognome = (SELECT cognomeRandom FROM RandomNames ORDER BY RAND() LIMIT 1)
where
	t.rc_p_cognome is not null and trim(t.rc_p_cognome) <> '' 
;
#rc_p_nome, rc_p_cognome
update
	vtiger_registrazione_clienti t
set 
    t.rc_p_nome = (SELECT n.nomeRandom FROM RandomNames n where n.cfPrimeSeiCifre = substring(t.rc_p_codice_fiscale,1,6)),
    t.rc_p_cognome = (SELECT n.cognomeRandom FROM RandomNames n where n.cfPrimeSeiCifre = substring(t.rc_p_codice_fiscale,1,6))
where
	t.rc_p_nome is not null and trim(t.rc_p_nome) <> '' 
    and t.rc_p_cognome is not null and trim(t.rc_p_cognome) <> '' 
    and t.rc_p_nome in (SELECT n.nomeRandom FROM RandomNames n)
    and t.rc_p_cognome in (SELECT n.cognomeRandom FROM RandomNames n) 
; 
#rc_p_email
update
	vtiger_registrazione_clienti t
set
	t.rc_p_email = 'fake@fake.com'
where
	t.rc_p_email is not null and trim(t.rc_p_email) <> ''
;
#rc_p_pec
update
	vtiger_registrazione_clienti t 
set
	t.rc_p_pec = 'fake@pec.com'
where
	t.rc_p_pec is not null and trim(t.rc_p_pec) <> ''
;
#rc_p_cellulare
update
	vtiger_registrazione_clienti t 
set
	t.rc_p_cellulare = '3123456789'
where
	t.rc_p_cellulare is not null and trim(t.rc_p_cellulare) <> ''
;
